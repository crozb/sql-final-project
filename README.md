# Postgres Project

## Setup Instructions
1. Using Git Bash, Clone the repository
2. Open pgAdmin and create a new database called **postgres_project**
3. Restore the dump file **project_dump.backup**


## Relational Databases

### Foreign Key Relationships

| Table Name  | Foreign Key (FK) Name  | Table Matching FK |
| --- | --- | --- |
| customers | store_id | stores |
|stores| address_id| address|
|inventory| item_id, store_id| items, stores|
|purchases| store_id, customer_id, payment_type_id| stores, customers, payment_types
|purchase_items| purchase_id, item_id| purchases, items|
### Multiple Choice

1. Users are not allowed to add purchases where the amount of items bought exceeds the amount of items in the store. This is because:
    1. There is a Trigger Function preventing new purchases that exceed the current inventory. /Here is the result "ERROR:  not enough inventory in stock
       CONTEXT:  PL/pgSQL function decrement() line 11 at RAISE"
2. Why will the following SQL statement not work in the stores database?
   >SELECT s.name AS "Store Name", a.street AS "Street Address", a.street2 AS "Suite", a.city AS "City", a.state AS "State", a.zip AS "Zip Code"
   >FROM stores s
   >INNER JOIN address a
   >ON address_id = a.id
    4. This is a valid query that will run without error.
3. What is the relationship between the stores and customers tables?
   2. Stores is a one-to-many relationship with Customers.
4. What is the relationship between the purchases and items tables?
    1. The tables have a many-to-many relationship with the purchase_items table acting as a mapping table.
    
       


##DIRECTIONS TO OPEN DUMP FILE
Included in the project files is a project_dump.backup file. In order to load this please follow the steps below.
1. Open and install postgreSQL and follow setup. Here is the link to install https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
   Here are instructions for installation via Catalyte https://internal.training.catalyte.io/mod/scorm/player.php?a=46&currentorg=Course_ID1_ORG&scoid=92
2. After installation, open up pgadmin 4 on your computer.
3. In the left border of the page click the arrow to drop down the menu from servers and then again from postgresSQL. You should now see "Databases"
4. Right click on databases
5. Click "create"
6. Name the database as you like
7. Once the database has been created right click on the database you created in the left side of the page and click restore
8. Within the restore window, direct the restore to the file you wish to restore from which is the backup/dump file.

