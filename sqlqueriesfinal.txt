1. SELECT name,street, city, state, zip  FROM
stores INNER JOIN address
ON stores.address_id = address.id

2.SELECT stores.name, sum(total_cost) AS "total sales"
FROM purchases 
INNER JOIN stores
ON stores.id = purchases.store_id
GROUP BY stores.id

3.SELECT name, CONCAT  (first_name, ' ', last_name) AS "Member name"
FROM stores 
INNER JOIN customers
ON stores.id = customers.store_id

4.SELECT stores.name, items.name AS "item name", inventory.quantity, items.price, items.notes
FROM stores
INNER JOIN inventory
ON  stores.id = inventory.store_id
INNER JOIN items
ON inventory.item_id = items.id
WHERE stores.name = 'Administaff, Inc.'

5.SELECT stores.name, purchases.total_cost AS "total sales in $", EXTRACT(MONTH FROM purchases.purchase_date) AS "Month"
FROM stores
INNER JOIN purchases
ON  stores.id = purchases.store_id
WHERE stores.name = 'Administaff, Inc.'
ORDER BY EXTRACT(MONTH FROM purchases.purchase_date) ASC

6.
SELECT stores.name, payment_types.payment_method, COUNT(payment_types.payment_method)AS "number of payment type"
FROM stores
INNER JOIN purchases
ON  stores.id = purchases.store_id
INNER JOIN payment_types
ON purchases.payment_type_id = payment_types.id
WHERE stores.name = 'Administaff, Inc.'
GROUP BY stores.name, payment_types.payment_method

7.SELECT stores.name, items.name, inventory.quantity
FROM stores
INNER JOIN inventory
ON  stores.id = inventory.store_id
INNER JOIN items
ON items.id = inventory.item_id
WHERE
inventory.quantity < 10

8.SELECT CONCAT  (customers.first_name, ' ', customers.last_name) AS "Member name", purchases.purchase_date, items.name AS "item purchased", payment_types.payment_method, purchase_items.quantity
FROM customers
INNER JOIN purchases
ON  customers.id = purchases.customer_id
INNER JOIN purchase_items
ON purchases.id = purchase_items.purchase_id
INNER JOIN items
ON purchase_items.item_id = items.id
INNER JOIN payment_types
ON    purchases.payment_type_id  = payment_types.id

9.UPDATE purchases
SET payment_type_id = 
	(
	SELECT payment_types.id
	FROM  payment_types
	WHERE 
	payment_method = 'credit')
FROM payment_types
WHERE purchases.payment_type_id = payment_types.id
and
payment_method = 'apple pay'

10. DELETE FROM payment_types WHERE payment_method = 'apple pay'

11.Insert into public."items"(name, price, notes)
VALUES ('Frosted Flakes', 5, 'They''re Grate!')

12.INSERT INTO inventory(store_id, item_id, quantity)
SELECT DISTINCT inventory.store_id (SELECT items.id FROM items WHERE items.name = 'Frosted Flakes'), 50
FROM inventory
INNER JOIN stores
on inventory.store_id = stores.id
INNER JOIN items 
on inventory.item_id = items.id
WHERE stores.name LIKE '%a%'

13.UPDATE items 
SET notes = 'They''re Gr-r-reat!'
WHERE notes = 'They''re Grate!'
